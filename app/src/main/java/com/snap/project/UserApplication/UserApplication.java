package com.snap.project.UserApplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.snap.project.ApplicationSuperUser.DetailActivity;
import com.snap.project.ApplicationSuperUser.ListItems;
import com.snap.project.ApplicationSuperUser.StartDetail;
import com.snap.project.R;

import java.util.ArrayList;
import java.util.Date;

public class UserApplication extends AppCompatActivity implements UserDetail{

    FirebaseDatabase database;
    DatabaseReference mRef;

    ArrayList<UserItem> listItems;
    UserAdapter myAdapter;

    RecyclerView recyclerViewUser;
    ProgressDialog dialog;

    static String value;

    Controller controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_application);

        dialog = new ProgressDialog(this);
        dialog.setTitle("Please Wait");
        dialog.setMessage("Fetching");
        dialog.show();
        database = FirebaseDatabase.getInstance();
        mRef = database.getReference();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);

        recyclerViewUser = (RecyclerView) findViewById(R.id.recyclerViewUser);
        recyclerViewUser.setHasFixedSize(true);
        recyclerViewUser.setLayoutManager(mLayoutManager);

        myAdapter = new UserAdapter();
        listItems = new ArrayList<>();

        controller = new Controller(this);

        String id = FirebaseAuth.getInstance().getUid();
        DatabaseReference application = database.getReference().child("ApplicationForm").child(id).child("Application");

        application.orderByChild("date").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()) {
                    String key = ds.getKey();
                    String id = FirebaseAuth.getInstance().getUid();
                    DatabaseReference keyRef = database.getReference().child("ApplicationForm").child(id).child("Application").child(key);
                    DatabaseReference checkLeave = database.getReference().child("ApplicationForm").child(id).child("NumberLeave");

                    checkLeave.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            value = dataSnapshot.getValue().toString();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    keyRef.orderByChild("from").addListenerForSingleValueEvent(new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String name = dataSnapshot.child("name").getValue(String.class);
                            String reason = dataSnapshot.child("reason").getValue(String.class);
                            long to = dataSnapshot.child("to").getValue(long.class);
                            Date date = new Date(to);
                            String getDate = date.toString();
                            long from = dataSnapshot.child("from").getValue(long.class);
                            Date datefrom = new Date(from);
                            String getDatefrom = datefrom.toString();
                            String rollno = dataSnapshot.child("rollno").getValue(String.class);
                            String type = dataSnapshot.child("type").getValue(String.class);
                            String totime = dataSnapshot.child("to_time").getValue(String.class);
                            String fromtime = dataSnapshot.child("from_time").getValue(String.class);
                            String approved = dataSnapshot.child("approved").getValue(String.class);
                            String numb = dataSnapshot.child("numb").getValue(String.class);

                            UserItem userItem = new UserItem(name, rollno, type, getDate, getDatefrom, reason, totime, fromtime, numb, approved);
                            listItems.add(userItem);
                            recyclerViewUser.setAdapter(myAdapter);
                            myAdapter.notifyDataSetChanged();
                            dialog.dismiss();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void startDetailActivity(String name_text, String rollno_text, String type_text, String date_to_long, String to_time, String date_from_long, String from_time, String reason_text, String status, String leaves) {
        Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
        intent.putExtra("name_text", name_text);
        intent.putExtra("rollno_text", rollno_text);
        intent.putExtra("type_text", type_text);
        intent.putExtra("date_to_long", date_to_long);
        intent.putExtra("to_time", to_time);
        intent.putExtra("date_from_long", date_from_long);
        intent.putExtra("from_time", from_time);
        intent.putExtra("reason_text", reason_text);
        intent.putExtra("leaves", leaves);
        intent.putExtra("status", status);
        startActivity(intent);
    }

    public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.user_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
           UserItem listItem = listItems.get(position);
            holder.title.setText(listItem.getName());
            holder.description.setText(listItem.getReason());
            holder.status.setText(listItem.getApproved());
            holder.numberleave.setText(listItem.getValue());
            String[] array_to = listItem.getTo().split(" ");
            holder.to.setText(array_to[0] + " " + array_to[1] + " " + array_to[2]);
            String[] array_from = listItem.getFrom().split(" ");
            holder.from.setText(array_from[0] + " " + array_from[1] + " " + array_from[2]);
        }

        @Override
        public int getItemCount() {
            return listItems.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            TextView title;
            TextView description;
            TextView to;
            TextView from;
            TextView status;
            TextView numberleave;

            LinearLayout linearLayout;

            public ViewHolder(View itemView) {
                super(itemView);

                title = (TextView) itemView.findViewById(R.id.title);
                description = (TextView) itemView.findViewById(R.id.description);
                to = (TextView) itemView.findViewById(R.id.to);
                from = (TextView) itemView.findViewById(R.id.from);
                status = (TextView) itemView.findViewById(R.id.status);
                numberleave = (TextView) itemView.findViewById(R.id.numberleave);

                linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout);
                linearLayout.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                UserItem userItem = listItems.get(this.getAdapterPosition());
                controller.onItemClick(userItem);
            }
        }
    }
}
