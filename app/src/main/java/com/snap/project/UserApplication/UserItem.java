package com.snap.project.UserApplication;

/**
 * Created by Shantanu on 2/11/2018.
 */

public class UserItem {

    String name;
    String rollno;
    String type;
    String to;
    String from;
    String reason;
    String totime;
    String fromtime;
    String value;
    String approved;

    public UserItem (String name, String rollno, String type, String to, String from, String reason, String totime, String fromtime, String value, String approved) {
        this.name = name;
        this.rollno = rollno;
        this.type = type;
        this.to = to;
        this.from = from;
        this.reason = reason;
        this.fromtime = fromtime;
        this.totime = totime;
        this.value = value;
        this.approved = approved;
    }

    public String getName() {
        return name;
    }

    public String getRollno() {
        return rollno;
    }

    public String getType() {
        return type;
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    public String getReason() {
        return reason;
    }

    public String getTotime() {
        return totime;
    }

    public String getFromtime() {
        return fromtime;
    }

    public String getValue() {
        return value;
    }
    public String getApproved() {
        return approved;
    }
}
