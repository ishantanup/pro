package com.snap.project.UserApplication;

/**
 * Created by Shantanu on 2/11/2018.
 */

public class Controller {
    UserDetail startDetail;

    public Controller(UserDetail startDetail) {
        this.startDetail = startDetail;
    }

    public void onItemClick(UserItem listItems) {
        startDetail.startDetailActivity(listItems.getName(), listItems.getRollno(),listItems.getType(), listItems.getTo(), listItems.getTotime(), listItems.getFrom(), listItems.getFromtime(), listItems.getReason(), listItems.getApproved(), listItems.getValue());
    }
}
