package com.snap.project.UserApplication;

/**
 * Created by Shantanu on 2/11/2018.
 */

public interface UserDetail {
    void startDetailActivity(String name_text, String rollno_text, String type_text, String date_to_long, String to_time, String date_from_long, String from_time, String reason_text, String status, String leaves);
}