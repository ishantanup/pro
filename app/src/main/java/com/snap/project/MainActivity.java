package com.snap.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.snap.project.ApplicationForm.FillForm;
import com.snap.project.ApplicationSuperUser.ListApplication;
import com.snap.project.UserApplication.UserApplication;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button fillForm;
    Button listForm;
    Button userForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fillForm = (Button) findViewById(R.id.fillForm);
        listForm = (Button) findViewById(R.id.listForm);
        userForm = (Button) findViewById(R.id.userForm);

        listForm.setVisibility(View.GONE);

        fillForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FillForm.class);
                startActivity(intent);
            }
        });

        userForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), UserApplication.class);
                startActivity(intent);
            }
        });

        listForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListApplication.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        if (email.equals("mam@hotmail.com") || email.equals("guard@hotmail.com") || email.equals("shanta@hotmail.com") || email.equals("hola@hola.com")) {
            listForm.setVisibility(View.VISIBLE);
            fillForm.setVisibility(View.GONE);
            userForm.setVisibility(View.GONE);
        }
    }
}
