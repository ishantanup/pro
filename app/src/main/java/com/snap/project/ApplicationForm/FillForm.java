package com.snap.project.ApplicationForm;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.snap.project.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FillForm extends AppCompatActivity {

    EditText name;
    EditText rollno;
    EditText type;
    EditText reason;
    EditText to;
    EditText hour_to;
    EditText hour_from;
    EditText from;
    Button submit;

    FirebaseDatabase database;
    DatabaseReference mRef;

    static String to_time, from_time;

    static String number;

    Calendar myCalendar;
    Calendar myCalendarTo;
    DatePickerDialog.OnDateSetListener date_to;
    DatePickerDialog.OnDateSetListener date_from;

    static long date_to_long, date_from_long;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_form);


        database = FirebaseDatabase.getInstance();
        mRef = database.getReference();

        try {
            String id = FirebaseAuth.getInstance().getUid();
            DatabaseReference checkLeave = database.getReference().child("ApplicationForm").child(id).child("NumberLeave");
            checkLeave.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        String value = dataSnapshot.getValue().toString();
                        String id = FirebaseAuth.getInstance().getUid();

                        String[] array = Calendar.getInstance().getTime().toString().split(" ");
                        if (Integer.parseInt(array[2]) == Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)) {
                            mRef.child("ApplicationForm")
                                    .child(id)
                                    .child("NumberLeave")
                                    .setValue("0");
                        }
                        if (Integer.parseInt(value) >= 6) {
                            final AlertDialog alertDialog = new AlertDialog.Builder(FillForm.this).create();
                            alertDialog.setTitle("You have Submitted 6 Request");
                            alertDialog.setMessage("As you know you cannot submit more that 3 Request so kindly submit one only in emergency");
                            alertDialog.setIcon(R.drawable.common_google_signin_btn_icon_dark_normal_background);
                            alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    alertDialog.cancel();
                                }
                            });

                            alertDialog.show();
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {
            Log.d("Exception", "" + e);
        }

        progressDialog = new ProgressDialog(this);

        name = (EditText) findViewById(R.id.name);
        rollno = (EditText) findViewById(R.id.rollno);
        type = (EditText) findViewById(R.id.type);
        reason = (EditText) findViewById(R.id.reason);
        to = (EditText) findViewById(R.id.to);
        from = (EditText) findViewById(R.id.from);
        hour_to = (EditText) findViewById(R.id.hour_to);
        hour_from = (EditText) findViewById(R.id.hour_from);

        hour_from.setOnClickListener(new View.OnClickListener() {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(FillForm.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                hour_from.setText(hourOfDay + ":" + minute);
                                from_time = hourOfDay + ":" + minute;
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        hour_to.setOnClickListener(new View.OnClickListener() {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(FillForm.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                hour_to.setText(hourOfDay + ":" + minute);
                                to_time = hourOfDay + ":" + minute;
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        myCalendar = Calendar.getInstance();
        myCalendarTo = Calendar.getInstance();

        date_from = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabelFrom();
            }

        };

        date_to = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendarTo.set(Calendar.YEAR, year);
                myCalendarTo.set(Calendar.MONTH, monthOfYear);
                myCalendarTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabelTo();
            }

        };

        from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(FillForm.this, date_from, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(FillForm.this, date_to, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        submit = (Button) findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToDatabase();
            }
        });
    }

    //private boolean checkX(String name_text, String rollno_text, String type_text, long to_text, long from_text, String reason_text, String to_time, String from_time) {
//        if (name_text.equals("")) {
//            message = "Name Required";
//            return false;
//        }
//        if (rollno_text.equals("")) {
//            message = "Roll No Required";
//            return false;
//        }
//        if (to_time.equals("")) {
//            message = "To time Required";
//            return false;
//        }
//        if (from_time.equals("")) {
//            message = "From time Required";
//            return false;
//        }
//        if (type_text.equals("")) {
//            message = "Type Required";
//            return false;
//        }
//        if (to_text == 0) {
//            message = "To Date Required";
//            return false;
//        }
//        if (from_text == 0) {
//            message = "From Date Required";
//            return false;
//        }
//        if (reason_text.equals("")) {
//            message = "Please Add a Reason";
//            return false;
//        }
   //     return true;
    //}

    public void saveToDatabase() {
        final String name_text = name.getText().toString();
        final String rollno_text = rollno.getText().toString();
        final String type_text = type.getText().toString();
        final String reason_text = reason.getText().toString();

        if (check(name_text, rollno_text, type_text, date_to_long, to_time, date_from_long, from_time, reason_text)) {
            progressDialog.setMessage("Posting");
            progressDialog.show();
            mRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String id = FirebaseAuth.getInstance().getUid();
                    long time = System.currentTimeMillis();
                    try {
                        String no = dataSnapshot.child("ApplicationForm")
                                .child(id)
                                .child("NumberLeave").getValue(String.class);

                        int leaveNumber = Integer.parseInt(no);

                        int incremented = leaveNumber + 1;
                        number = Integer.toString(incremented);
                        mRef.child("ApplicationForm")
                                .child(id)
                                .child("NumberLeave")
                                .setValue(number);
                    } catch (Exception e) {
                        mRef.child("ApplicationForm")
                                .child(id)
                                .child("NumberLeave")
                                .setValue("1");
                        number = "1";
                    }
                    ApplicationModel applicationModel = new ApplicationModel(name_text, rollno_text, type_text, date_to_long, to_time, date_from_long, from_time, reason_text, time, "NA", id, number);
                    mRef.child("ApplicationForm")
                            .child(id)
                            .child("Application")
                            .child(id + reason_text)
                            .setValue(applicationModel);
                    mRef.child("ApplicationForm")
                            .child(id)
                            .child("from")
                            .setValue(date_from_long);
                    mRef.child("ApplicationForm")
                            .child(id)
                            .child("date")
                            .setValue(time);
                    progressDialog.dismiss();
                    Toast.makeText(FillForm.this, "Application Submited", Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), "" + databaseError, Toast.LENGTH_SHORT).show();
                }
            });
        }


//        if (check(name_text, rollno_text, type_text, date_to_long, date_from_long, reason_text, to_time, from_time)) {
//
//        } else {
//            Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
//        }
    }

    private boolean check(String name_text, String rollno_text, String type_text, long date_to_long, String to_time, long date_from_long, String from_time, String reason_text) {
        if (name_text.equals("") || rollno_text.equals("") || type_text.equals("") || to_time.equals("") || from_time.equals("") || reason_text.equals("")) {
            Toast.makeText(this, "All Fields are Required", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private void updateLabelFrom() {
        String myFormat = "dd/MM/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        from.setText(sdf.format(myCalendar.getTimeInMillis()));
        date_from_long = myCalendar.getTimeInMillis();
    }

    private void updateLabelTo() {
        String myFormat = "dd/MM/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        to.setText(sdf.format(myCalendarTo.getTimeInMillis()));
        date_to_long = myCalendarTo.getTimeInMillis();
    }
}