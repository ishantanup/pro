package com.snap.project.ApplicationForm;

/**
 * Created by user on 09/02/2018.
 */

public class ApplicationModel {

    String name;
    String rollno;
    String type;
    long to;
    long from;
    String reason;
    long date;
    String to_time;
    String from_time;
    String approved;
    String uid;
    String numb;

    public ApplicationModel(String name, String rollno, String type, long to, String to_time, long from,String from_time, String reason, long date, String approved, String uid, String
                             numb) {
        this.name = name;
        this.rollno = rollno;
        this.type = type;
        this.to = to;
        this.from = from;
        this.reason = reason;
        this.date = date;
        this.from_time = from_time;
        this.to_time = to_time;
        this.approved = approved;
        this.uid = uid;
        this.numb = numb;
    }

    public String getName() {
        return name;
    }

    public String getRollno() {
        return rollno;
    }

    public String getType() {
        return type;
    }

    public long getTo() {
        return to;
    }

    public long getFrom() {
        return from;
    }

    public String getReason() {
        return reason;
    }

    public long getDate() {
        return date;
    }

    public String getTo_time() {
        return to_time;
    }

    public String getFrom_time() {
        return from_time;
    }

    public String getApproved() {
        return approved;
    }

    public String getUid() {
        return uid;
    }

    public String getNumb() {
        return numb;
    }
}
