package com.snap.project.ApplicationSuperUser;

/**
 * Created by Shantanu on 2/10/2018.
 */

public interface StartDetail {
    void startDetailActivity(String name_text, String rollno_text, String type_text, String date_to_long, String to_time, String date_from_long, String from_time, String reason_text, String status, String leaves);
}
