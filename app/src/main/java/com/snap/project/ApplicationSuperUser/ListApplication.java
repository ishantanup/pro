package com.snap.project.ApplicationSuperUser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.snap.project.R;

import java.util.ArrayList;
import java.util.Date;

public class ListApplication extends AppCompatActivity implements StartDetail{

    RecyclerView recyclerView;
    ArrayList<ListItems> listItems;
    MyAdapter myAdapter;

    FirebaseDatabase database;
    DatabaseReference mRef;

    ProgressDialog dialog;

    Controller controller;

    static String value, val;
    static int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_application);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);

        listItems = new ArrayList<>();

        dialog = new ProgressDialog(this);

        controller = new Controller(this);

        database = FirebaseDatabase.getInstance();
        mRef = database.getReference().child("ApplicationForm");

        dialog.setTitle("Please Wait");
        dialog.setMessage("Fetching");
        dialog.show();
        mRef.orderByChild("date").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    final String key = ds.getKey();
                    final DatabaseReference ref = mRef.child(key);
                    ref.orderByChild("date").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final DatabaseReference refRef = ref.child("Application");
                            refRef.orderByChild("date").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for(DataSnapshot ds : dataSnapshot.getChildren()) {
                                        String innerKey = ds.getKey();
                                        DatabaseReference innerKeyRef = refRef.child(innerKey);
                                        innerKeyRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                String name = dataSnapshot.child("name").getValue(String.class);
                                                String reason = dataSnapshot.child("reason").getValue(String.class);
                                                long to = dataSnapshot.child("to").getValue(long.class);
                                                Date date = new Date(to);
                                                String getDate = date.toString();
                                                long from = dataSnapshot.child("from").getValue(long.class);
                                                Date datefrom = new Date(from);
                                                String getDatefrom = datefrom.toString();
                                                String number = dataSnapshot.child("numb").getValue(String.class);
                                                String rollno = dataSnapshot.child("rollno").getValue(String.class);
                                                String type = dataSnapshot.child("type").getValue(String.class);
                                                String totime = dataSnapshot.child("to_time").getValue(String.class);
                                                String fromtime = dataSnapshot.child("from_time").getValue(String.class);
                                                String approved = dataSnapshot.child("approved").getValue(String.class);
                                                String uid = dataSnapshot.child("uid").getValue(String.class);
                                                ListItems listItem = new ListItems(name, rollno, type, getDate, getDatefrom, reason, totime, fromtime, number, approved, uid);
                                                listItems.add(listItem);
                                                recyclerView.setAdapter(myAdapter);
                                                myAdapter.notifyDataSetChanged();
                                                dialog.dismiss();
                                            }
                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        myAdapter = new MyAdapter();
        recyclerView.setAdapter(myAdapter);
    }

    @Override
    public void startDetailActivity(String name_text, String rollno_text, String type_text, String date_to_long, String to_time, String date_from_long, String from_time, String reason_text,  String status, String leaves) {
        Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
        intent.putExtra("name_text", name_text);
        intent.putExtra("rollno_text", rollno_text);
        intent.putExtra("type_text", type_text);
        intent.putExtra("date_to_long", date_to_long);
        intent.putExtra("to_time", to_time);
        intent.putExtra("date_from_long", date_from_long);
        intent.putExtra("from_time", from_time);
        intent.putExtra("reason_text", reason_text);
        intent.putExtra("leaves", leaves);
        intent.putExtra("status", status);
        startActivity(intent);
    }
    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.app_item, parent, false);
            return new ViewHolder(view);
        }
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final ListItems listItem = listItems.get(position);
            holder.title.setText(listItem.getName());
            holder.description.setText(listItem.getReason());
            holder.status.setText(listItem.getApproved());
            holder.numberleave.setText(listItem.getValue());
            String[] array_to = listItem.getTo().split(" ");
            holder.to.setText(array_to[0] + " " + array_to[1] + " " + array_to[2]);
            String[] array_from = listItem.getFrom().split(" ");
            holder.from.setText(array_from[0] + " " + array_from[1] + " " + array_from[2]);

            holder.approvebutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String id = FirebaseAuth.getInstance().getUid();
                    final DatabaseReference recRef = database.getReference().child("ApplicationForm").child(listItem.getUid()).child("Application").child(listItem.getUid() + listItem.getReason()).child("approved");
                    recRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            val = dataSnapshot.getValue().toString();
                            if (val.equals("NA") || val.equals("Rejected")) {
                                recRef.setValue("Approved");
                                recRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        String valueX = dataSnapshot.getValue().toString();
                                        holder.status.setText(valueX);
                                        holder.approvebutton.setText(valueX);
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });

                }
            });

            holder.rejectbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String id = FirebaseAuth.getInstance().getUid();
                    final DatabaseReference recRef = database.getReference().child("ApplicationForm").child(listItem.getUid()).child("Application").child(listItem.getUid() + listItem.getReason()).child("approved");
                    recRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            val = dataSnapshot.getValue().toString();
                            if (val.equals("NA") || val.equals("Approved")) {
                                recRef.setValue("Rejected");
                                recRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        String valueX = dataSnapshot.getValue().toString();
                                        holder.status.setText(valueX);
                                        holder.rejectbutton.setText(valueX);
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });

                }
            });
        }

        @Override
        public int getItemCount() {
            return listItems.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            TextView title;
            TextView description;
            TextView to;
            TextView from;
            TextView status;
            TextView numberleave;


            Button approvebutton;
            Button rejectbutton;

            LinearLayout linearLayout;

            public ViewHolder(View itemView) {
                super(itemView);

                title = (TextView) itemView.findViewById(R.id.title);
                description = (TextView) itemView.findViewById(R.id.description);
                to = (TextView) itemView.findViewById(R.id.to);
                from = (TextView) itemView.findViewById(R.id.from);
                status = (TextView) itemView.findViewById(R.id.status);
                numberleave = (TextView) itemView.findViewById(R.id.numberleave);
                approvebutton = (Button) itemView.findViewById(R.id.approvebutton);
                rejectbutton = (Button) itemView.findViewById(R.id.rejectbutton);
                approvebutton.setVisibility(View.GONE);
                rejectbutton.setVisibility(View.GONE);

                String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                if (email.equals("shanta@hotmail.com")) {
                    approvebutton.setVisibility(View.VISIBLE);
                    rejectbutton.setVisibility(View.VISIBLE);
                }
                linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout);
                linearLayout.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                ListItems listItem = listItems.get(this.getAdapterPosition());
                controller.onItemClick(listItem);
            }
        }
    }
}
