package com.snap.project.ApplicationSuperUser;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.snap.project.R;

public class DetailActivity extends AppCompatActivity {

    TextView name_text;
    TextView rollno_text;
    TextView type_text;
    TextView date_to_long;
    TextView to_time;
    TextView date_from_long;
    TextView from_time;
    TextView reason_text;
    TextView status_detail;
    TextView leaves;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        name_text = (TextView) findViewById(R.id.name_text);
        rollno_text = (TextView) findViewById(R.id.rollno_text);
        type_text = (TextView) findViewById(R.id.type_text);
        date_to_long = (TextView) findViewById(R.id.date_to_long);
        to_time = (TextView) findViewById(R.id.to_time);
        date_from_long = (TextView) findViewById(R.id.date_from_long);
        from_time = (TextView) findViewById(R.id.from_time);
        reason_text = (TextView) findViewById(R.id.reason_text);
        leaves = (TextView) findViewById(R.id.leaves);
        status_detail = (TextView) findViewById(R.id.status_detail);

        Intent intent = getIntent();
        String name_text_text = intent.getStringExtra("name_text");
        String rollno_text_text = intent.getStringExtra("rollno_text");
        String type_text_text = intent.getStringExtra("type_text");
        String date_to_long_text = intent.getStringExtra("date_to_long");
        String to_time_text = intent.getStringExtra("to_time");
        String date_from_long_text = intent.getStringExtra("date_from_long");
        String from_time_text = intent.getStringExtra("from_time");
        String reason_text_text = intent.getStringExtra("reason_text");
        String leaves_text = intent.getStringExtra("leaves");
        String status_text = intent.getStringExtra("status");

        name_text.setText(name_text_text);
        rollno_text.setText(rollno_text_text);
        type_text.setText(type_text_text);
        leaves.setText(leaves_text);
        status_detail.setText(status_text);
        String[] array_to = date_to_long_text.split(" ");
        date_to_long.setText(array_to[0] + " " + array_to[1] + " " + array_to[2]);
        String[] array_from = date_from_long_text.split(" ");
        to_time.setText(to_time_text);
        date_from_long.setText(array_from[0] + " " + array_from[1] + " " + array_from[2]);
        from_time.setText(from_time_text);
        reason_text.setText(reason_text_text);
    }
}
