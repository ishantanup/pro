package com.snap.project.ApplicationSuperUser;

/**
 * Created by Shantanu on 2/10/2018.
 */

public class Controller {
    StartDetail startDetail;

    public Controller(StartDetail startDetail) {
        this.startDetail = startDetail;
    }

    public void onItemClick(ListItems listItems) {
        startDetail.startDetailActivity(listItems.getName(), listItems.getRollno(),listItems.getType(), listItems.getTo(), listItems.getTotime(), listItems.getFrom(), listItems.getFromtime(), listItems.getReason(), listItems.getApproved(), listItems.getValue());
    }
}
